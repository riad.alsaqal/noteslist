import { StyleSheet, Text, View } from "react-native";
import { StatusBar } from "expo-status-bar";
import { Navigator } from "./src/navigation";
import { useInitDb } from "./src/hooks";
import { SafeAreaView } from "react-native-safe-area-context";
const Index = () => {
  const init = useInitDb();
  init();
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <StatusBar style="auto" />
        <Navigator />
      </View>
    </SafeAreaView>
  );
};

export default Index;

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
  },
});
