import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { HomeScreen, NoteFormScreen, NoteScreen } from "../screens";
import { TStack } from "../types";
const Stack = createNativeStackNavigator<TStack>();
export const ListStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="NoteForm" component={NoteFormScreen} />
      <Stack.Screen name="NoteScreen" component={NoteScreen} />
    </Stack.Navigator>
  );
};
