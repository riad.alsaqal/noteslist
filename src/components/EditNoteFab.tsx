import React from "react";
import { FAB } from "react-native-paper";
import { useNavigation } from "../hooks";
import { TNote } from "../types";
export const EditNoteFab: React.FC<TProps> = ({ noteData }) => {
  const { navigate } = useNavigation();
  return (
    <FAB
      icon={"pencil"}
      style={{ backgroundColor: "lightgray" }}
      onPress={() => {
        navigate("NoteForm", { Note: noteData });
      }}
    />
  );
};

type TProps = {
  noteData: TNote;
};
