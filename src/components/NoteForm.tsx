import React, { useRef } from "react";
import {
  TextInput,
  Avatar,
  Button,
  IconButton,
  Text,
} from "react-native-paper";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { useNoteForm, useRoute } from "../hooks";
import { RequiredWrapper } from "./RequiredWrapper";
import { ImageActionSheet } from "./ImageActionSheet";
import { ActionSheetRef } from "react-native-actions-sheet";
import { NoteDropDown } from "./NoteDropDown";
import { SaveToGallery } from "./SaveToGallery";
import { ErrorMessage } from "./ErrorMessage";

export const NoteForm = () => {
  const { params } = useRoute<"NoteForm">();
  const [openDialog, setOpenDialog] = React.useState<string>("");
  const noteForm = useNoteForm(params?.Note);
  const ref = useRef<ActionSheetRef>(null);
  const open = () => {
    ref.current?.show();
  };
  return (
    <View style={style.View}>
      <TouchableOpacity
        onPress={open}
        style={{
          backgroundColor: "yellow",
          borderRadius: 75,
          overflow: "hidden",
        }}
      >
        <Avatar.Image
          source={
            noteForm.values.image
              ? { uri: noteForm.values.image }
              : require("../../assets/NotesLogo.png")
          }
          size={150}
        />
        {!noteForm.values.image && (
          <View
            style={{
              backgroundColor: "lightgray",
              width: "38%",
              position: "absolute",
              height: 40,
              right: 0,
              bottom: 0,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <IconButton icon="camera" size={40} iconColor="gray" />
          </View>
        )}
      </TouchableOpacity>
      <ErrorMessage
        message={noteForm.errors.image}
        touched={noteForm.touched.image}
      />
      <TextInput
        placeholder="title"
        value={noteForm.values.title}
        onChangeText={noteForm.handleChange("title")}
        style={style.field}
      />
      <ErrorMessage
        message={noteForm.errors.title}
        touched={noteForm.touched.title}
      />

      <RequiredWrapper style={style.field}>
        <TextInput
          placeholder="description"
          value={noteForm.values.description}
          onBlur={() => noteForm.setFieldTouched("description")}
          onChangeText={noteForm.handleChange("description")}
        />
      </RequiredWrapper>

      <ErrorMessage
        message={noteForm.errors.description}
        touched={noteForm.touched.description}
      />

      <NoteDropDown
        style={{ ...style.field }}
        setValue={(value) => {
          noteForm.handleChange("parent_id")(value);
        }}
        noteId={params?.Note.id as string}
        value={noteForm.values.parent_id?.toString() ?? ""}
      />
      <ErrorMessage
        message={noteForm.errors.parent_id}
        touched={noteForm.touched.parent_id}
      />

      <Button mode="contained" onPress={() => noteForm.handleSubmit()}>
        save
      </Button>
      <SaveToGallery
        visible={openDialog}
        hideDialog={() => {
          setOpenDialog("");
        }}
      />
      <ImageActionSheet
        reff={ref}
        onPick={({ url, from, base64 }) => {
          ref.current?.hide();
          noteForm.handleChange("image")(base64);
          if (from === "camera") {
            setOpenDialog(url);
          }
        }}
      />
    </View>
  );
};

const style = StyleSheet.create({
  View: {
    justifyContent: "center",
    alignItems: "center",
    gap: 20,
    margin: 5,
  },
  field: {
    width: "80%",
  },
});
