import React from "react";
import { StyleSheet } from "react-native";
import { FAB } from "react-native-paper";
import { useNavigation } from "../hooks";
export const AddNote = () => {
  const { navigate } = useNavigation();
  return (
    <FAB icon="plus" style={styles.fab} onPress={() => navigate("NoteForm")} />
  );
};

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    margin: 15,
    bottom: 0,
    left: 0,
    backgroundColor: "lightgreen",
  },
});
