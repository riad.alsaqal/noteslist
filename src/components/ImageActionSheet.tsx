import React from "react";
import ActionSheet, { ActionSheetRef } from "react-native-actions-sheet";
import { Button } from "react-native-paper";
import { View } from "react-native";
import { pickImage, takeImage } from "../utils";

export const ImageActionSheet: React.FC<TProps> = ({ reff, onPick }) => {
  return (
    <View>
      <ActionSheet
        containerStyle={{ height: "50%", flexDirection: "row" }}
        ref={reff}
        useBottomSafeAreaPadding
      >
        <Button
          icon="view-gallery"
          onPress={() => {
            pickImage(onPick);
          }}
        >
          gallery
        </Button>
        <Button
          icon="camera"
          onPress={() => {
            takeImage(onPick);
          }}
        >
          camera
        </Button>
      </ActionSheet>
    </View>
  );
};

type TProps = {
  reff: React.RefObject<ActionSheetRef>;
  onPick: ({
    url,
    from,
    base64,
  }: {
    url: string;
    from: "camera" | "gallery";
    base64: string;
  }) => void;
};
