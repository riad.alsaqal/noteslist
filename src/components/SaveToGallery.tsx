import React from "react";
import { Button, Dialog, Portal, Text } from "react-native-paper";
import * as MediaLibrary from "expo-media-library";
export const SaveToGallery: React.FC<TProps> = ({ visible, hideDialog }) => {
  const saveImage = async () => {
    try {
      const result = await MediaLibrary.saveToLibraryAsync(visible);
    } catch (error) {}
  };
  return (
    <Portal>
      <Dialog visible={!!visible} onDismiss={hideDialog}>
        <Dialog.Title>save image</Dialog.Title>
        <Dialog.Content>
          <Text variant="bodyMedium">
            do you want to save this image in gallery?
          </Text>
        </Dialog.Content>
        <Dialog.Actions>
          <Button
            onPress={() => {
              saveImage();
              hideDialog();
            }}
          >
            ok
          </Button>
          <Button onPress={hideDialog}>no</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
};

type TProps = {
  visible: string;
  hideDialog: () => void;
};
