import React from "react";
import { Card, Text } from "react-native-paper";

import { TNote } from "../types";
export const NoteCard: React.FC<TProps> = ({ cardData, children }) => {
  const { description, id, parent_id, image, title } = cardData;
  return (
    <Card style={{ margin: 10 }}>
      <Card.Content>
        <Text variant="titleLarge">{title}</Text>
        <Card.Cover
          source={{ uri: image ?? require("../../assets/NotesLogo.png") }}
        />

        <Text variant="bodyMedium">{description}</Text>
      </Card.Content>
      <Card.Actions style={{ gap: 20 }}>{children}</Card.Actions>
    </Card>
  );
};

type TProps = {
  cardData: TNote;
  children?: React.ReactElement;
};
