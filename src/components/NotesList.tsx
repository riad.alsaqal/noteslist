import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { TouchableRipple } from "react-native-paper";
import { useIsFocused } from "@react-navigation/native";
import { NoteCard } from "./NoteCard";
import { useLoadNotes, useNavigation } from "../hooks";
import { TNote } from "../types";
import { uniqueId } from "lodash";
import { EditNoteFab } from "./EditNoteFab";
import { DeleteNote } from "./DeleteNoteFab";

export const NotesList = () => {
  const [notes, setNotes] = React.useState<TNote[]>([]);
  const [page, setPage] = React.useState(0);
  const isFoucuse = useIsFocused();
  const { navigate } = useNavigation();
  const handelSetNotes = (newNotes: TNote[]) => {
    setNotes([...notes, ...newNotes]);
  };
  const handleGetNotes = (newNotes: TNote[]) => {
    setNotes([...newNotes]);
  };
  const handleDeleteNote = (noteId: number) => {
    setNotes(() => notes.filter((note) => Number(note.id) !== noteId));
  };
  const loadNotes = useLoadNotes(handelSetNotes);
  const getNotes = useLoadNotes(handleGetNotes);
  React.useEffect(() => {
    getNotes(0);
  }, [isFoucuse]);
  return (
    <View>
      <FlatList
        data={notes}
        renderItem={({
          item: { description, id, parent_id, image, title },
        }) => (
          <TouchableRipple
            onPress={() =>
              navigate("NoteScreen", {
                description,
                id,
                parent_id,
                image,
                title,
              })
            }
          >
            <NoteCard
              cardData={{
                title,
                id,
                parent_id,
                description,
                image,
              }}
              children={
                <>
                  <EditNoteFab
                    noteData={{
                      title,
                      id,
                      parent_id,
                      description,
                      image,
                    }}
                  />
                  <DeleteNote noteId={Number(id)} onDelete={handleDeleteNote} />
                </>
              }
            />
          </TouchableRipple>
        )}
        onEndReached={() => {
          loadNotes(page + 1);
          setPage(page + 1);
        }}
      />
    </View>
  );
};

const style = StyleSheet.create({
  View: {},
});
