import React from "react";
import { FAB, useTheme } from "react-native-paper";
import { useDeleteNote } from "../hooks";

export const DeleteNote: React.FC<TProps> = ({ noteId, onDelete }) => {
  const theme = useTheme();
  const deleteNote = useDeleteNote();
  return (
    <FAB
      icon="trash-can"
      style={{ backgroundColor: theme.colors.error }}
      color={theme.colors.background}
      onPress={() => {
        deleteNote(noteId);
        onDelete(noteId);
      }}
    />
  );
};
type TProps = {
  noteId: number;
  onDelete: (noteId: number) => void;
};
