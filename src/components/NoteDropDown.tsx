import React from "react";
import DropDownPicker from "react-native-dropdown-picker";
import { StyleProp } from "react-native/Libraries/StyleSheet/StyleSheet";
import { ViewStyle } from "react-native/Libraries/StyleSheet/StyleSheetTypes";
import { useGetNotes } from "../hooks/useGetNotes";

export const NoteDropDown: React.FC<TProps> = ({
  setValue,
  value,
  style,
  noteId,
}) => {
  const [open, setOpen] = React.useState(false);

  const { notes } = useGetNotes();
  return (
    <DropDownPicker
      modalTitle="pick parent note"
      containerStyle={{
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
        zIndex: open ? 1 : 0,
      }}
      dropDownContainerStyle={{ width: "80%" }}
      style={style}
      open={open}
      setOpen={setOpen}
      items={notes
        .filter((note) => Number(note.id) !== Number(noteId))
        .map((item) => ({
          label: item.title ?? item.description,
          value: item.id.toString(),
        }))}
      setValue={(e) => {
        console.log("aaa", e());
        setValue(e() as string);
      }}
      value={value}
      dropDownDirection="AUTO"
      bottomOffset={100}
    />
  );
};

type TProps = {
  value: string;
  setValue: (value: string) => void;
  style?: StyleProp<ViewStyle>;
  noteId: string;
};
