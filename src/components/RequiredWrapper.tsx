import React from "react";
import { ViewProps, View, Text, StyleSheet } from "react-native";

export const RequiredWrapper: React.FC<TProps> = ({ children, ...props }) => {
  return (
    <View {...props}>
      <Text style={style.Text}>*</Text>
      {children}
    </View>
  );
};

const style = StyleSheet.create({
  Text: {
    color: "red",
    alignSelf: "flex-end",
  },
});

type TProps = {
  children: React.ReactElement;
} & ViewProps;
