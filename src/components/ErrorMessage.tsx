import React from "react";
import { Text } from "react-native-paper";

export const ErrorMessage: React.FC<TProps> = ({ message, touched }) => {
  return !!message && touched ? (
    <Text style={{ color: "red" }}>{message} </Text>
  ) : (
    <></>
  );
};

type TProps = {
  message?: string;
  touched?: boolean;
};
