import * as ImagePicker from "expo-image-picker";

export const pickImage = async (onPick: onPick) => {
  let result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.All,
    allowsEditing: true,
    aspect: [4, 3],
    quality: 1,
    base64: true,
  });

  if (!result.canceled) {
    onPick({
      url: result.assets[0].uri,
      from: "gallery",
      base64: "data:image/jpeg;base64," + result.assets[0].base64,
    });
  }
};

export const takeImage = async (onPick: onPick) => {
  const permission = await ImagePicker.requestCameraPermissionsAsync();

  if (permission.granted) {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
      base64: true,
    });

    if (!result.canceled) {
      onPick({
        url: result.assets[0].uri,
        from: "camera",
        base64: "data:image/jpeg;base64," + result.assets[0].base64,
      });
    }
  }
};

type onPick = ({
  url,
  from,
  base64,
}: {
  url: string;
  from: "camera" | "gallery";
  base64: string;
}) => void;
