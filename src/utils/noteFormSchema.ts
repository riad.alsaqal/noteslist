import * as Yup from "yup";

export const noteFormShema = Yup.object({
  title: Yup.string(),
  image: Yup.string().required("image is required"),
  description: Yup.string().required(" description is required"),
  parentId: Yup.string(),
});
