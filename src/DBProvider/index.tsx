import React, { createContext, useContext } from "react";
import * as SQLite from "expo-sqlite";
import { WebSQLDatabase } from "expo-sqlite";

const db = SQLite.openDatabase("notes.db");

const DBContext = createContext<WebSQLDatabase>(db);

export const DBProvider: React.FC<TProps> = ({ children }) => {
  return <DBContext.Provider value={db}>{children}</DBContext.Provider>;
};

export const useDB = () => {
  const db = useContext(DBContext);
  return db;
};

type TProps = {
  children: React.ReactElement;
};
