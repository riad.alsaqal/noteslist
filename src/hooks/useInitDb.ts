import React from "react";
import { useDB } from "../DBProvider";

export const useInitDb = () => {
  const db = useDB();
  const initializeDatabase = () => {
    db.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY, title TEXT, description TEXT NOT NULL, image BLOB NOT NULL, parent_id INTEGER)"
      );
    });
  };

  return initializeDatabase;
};
