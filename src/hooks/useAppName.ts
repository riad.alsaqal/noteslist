import React, { useEffect } from "react";
import { getLocales } from "expo-localization";

export const useAppName = () => {
  const [name, setName] = React.useState<string>();

  useEffect(() => {
    getLocales()[0].languageCode === "ar"
      ? setName("قائمة الملاحظات")
      : setName("Notes List");
  }, []);
  return name;
};
