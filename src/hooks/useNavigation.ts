import React from "react";
import { TStack } from "../types";
import {
  useNavigation as useRN,
  NavigationProp,
} from "@react-navigation/native";

export const useNavigation = () => {
  const navigation = useRN<NavigationProp<TStack>>();
  navigation;
  return navigation;
};
