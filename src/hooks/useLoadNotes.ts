import React from "react";
import { useDB } from "../DBProvider";
import { TNote } from "../types";

export const useLoadNotes = (setNotes: (newNotes: TNote[]) => void) => {
  const db = useDB();
  const loadNotes = (page: number) => {
    const offset = page * 10;
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM notes ORDER BY id DESC LIMIT 10 OFFSET ?",
        [offset],
        (_, { rows }) => {
          setNotes(rows._array);
        },
        (_, error): any => {}
      );
    });
  };
  return loadNotes;
};
