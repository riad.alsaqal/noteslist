import React from "react";
import { useDB } from "../DBProvider";
import { TNote } from "../types";

export const useGetNote = (noteId: number) => {
  const [note, setNote] = React.useState<TNote>();
  const db = useDB();
  const getNote = () => {
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM notes WHERE id = ? LIMIT 1",
        [noteId],
        (_, { rows }) => {
          if (rows.length > 0) {
            const note = rows.item(0);
            setNote(note);
          } else {
          }
        },
        (_, error): any => {
          console.log(`Error getting note with id ${noteId}:`, error);
        }
      );
    });
  };
  React.useEffect(() => {
    getNote(), [];
  });

  return { note };
};
