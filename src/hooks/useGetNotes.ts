import React from "react";
import { useDB } from "../DBProvider";
import { TNote } from "../types";

export const useGetNotes = () => {
  const [notes, setNotes] = React.useState<TNote[]>([]);
  const db = useDB();
  const getNotes = () => {
    db.transaction((tx) => {
      tx.executeSql(
        "SELECT * FROM notes",
        [],
        (_, { rows }) => {
          setNotes(rows._array as TNote[]);
        },
        (_, error): any => {}
      );
    });
  };
  React.useEffect(() => {
    getNotes();
  }, []);
  return { notes };
};
