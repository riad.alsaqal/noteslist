import { useDB } from "../DBProvider";
import { TNote } from "../types";

export const useInsertNote = () => {
  const db = useDB();

  const insertNote = ({ description, image, parent_id, title }: TNote) => {
    db.transaction((tx) => {
      tx.executeSql(
        "INSERT INTO notes (title, description, image, parent_id) VALUES (?, ?, ?, ?)",
        [title as string, description, image, parent_id as number],
        (_, { rowsAffected, insertId }) => {},
        (_, error): any => {}
      );
    });
  };
  return insertNote;
};
