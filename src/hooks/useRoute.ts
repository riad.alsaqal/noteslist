import React from "react";
import { useRoute as useRNRoute, RouteProp } from "@react-navigation/native";
import { TStack } from "../types";

export const useRoute = <T extends keyof TStack>() => {
  const params = useRNRoute<RouteProp<TStack,T>>();
  return params;
};
