import { useFormik } from "formik";
import { TNote } from "../types";
import { noteFormShema } from "../utils";
import { useInsertNote } from "./useInsertNote";
import { useNavigation } from "./useNavigation";
import { useUpdateNote } from "./useUpdateNote";
const initialValues: TNote = {
  id: "",
  parent_id: -1,
  description: "",
  image: "",
  title: "",
};

export const useNoteForm = (values?: TNote) => {
  const insertNote = useInsertNote();
  const updateNote = useUpdateNote();
  const { goBack } = useNavigation();

  const noteForm = useFormik<TNote>({
    initialValues: values?.description ? values : initialValues,
    onSubmit: ({ description, id, parent_id, image, title }) => {
      console.log("parent", parent_id);
      values?.description
        ? updateNote({
            description,
            image: image as string,
            parent_id: Number(parent_id) ?? undefined,
            title,
            id,
          })
        : insertNote({
            description,
            image: image as string,
            parent_id: parent_id ?? undefined,
            title,
            id,
          });
      goBack();
    },
    validationSchema: noteFormShema,
  });
  return noteForm;
};
