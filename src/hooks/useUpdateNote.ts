import React from "react";
import { useDB } from "../DBProvider";
import { TNote } from "../types";

export const useUpdateNote = () => {
  const db = useDB();

  const updateNote = ({ description, id, image, parent_id, title }: TNote) => {
    db.transaction((tx) => {
      tx.executeSql(
        "UPDATE notes SET title = ?, description = ?, image = ?, parent_id =? WHERE id = ?",
        [title as string, description, image, Number(parent_id) as number, id],
        (_, { rowsAffected }) => {
          if (rowsAffected > 0) {
          } else {
          }
        },
        (_, error): any => {}
      );
    });
  };
  return updateNote;
};
