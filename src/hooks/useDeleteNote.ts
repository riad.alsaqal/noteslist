import React from "react";
import { useDB } from "../DBProvider";

export const useDeleteNote = () => {
  const db = useDB();
  const deleteNote = (noteId: number) => {
    db.transaction((tx) => {
      tx.executeSql(
        "DELETE FROM notes WHERE id = ?",
        [noteId],
        (_, { rowsAffected }) => {
          if (rowsAffected > 0) {
          } else {
          }
        },
        (_, error): any => {}
      );
    });
  };
  return deleteNote;
};
