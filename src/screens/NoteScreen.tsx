import React from "react";
import { View } from "react-native";
import { Text } from "react-native-paper";
import { NoteCard } from "../components";
import { useGetNote, useRoute } from "../hooks";
import { TNote } from "../types";
export const NoteScreen = () => {
  const { params } = useRoute<"NoteScreen">();
  const { note } = useGetNote(Number(params.parent_id));

  return (
    <View>
      <NoteCard cardData={params} />
      {note?.description ? (
        <>
          <Text> Parent Note</Text>
          <NoteCard cardData={note as TNote} />
        </>
      ) : (
        <Text> this Note don't have parent</Text>
      )}
    </View>
  );
};
