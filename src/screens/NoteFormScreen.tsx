import React from "react";
import { View } from "react-native";
import { NoteForm } from "../components";
export const NoteFormScreen = () => {
  return (
    <View>
      <NoteForm />
    </View>
  );
};
