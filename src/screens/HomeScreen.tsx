import { View, StyleSheet } from "react-native";
import { Text } from "react-native-paper";
import { AddNote, NotesList } from "../components";
import { useAppName } from "../hooks";
export const HomeScreen = () => {
  const appName = useAppName();
  return (
    <View style={style.View}>
      <Text variant="headlineLarge">{appName}</Text>
      <NotesList />
      <AddNote />
    </View>
  );
};

const style = StyleSheet.create({
  View: {
    flexDirection: "column",
    flex: 1,
  },
});
