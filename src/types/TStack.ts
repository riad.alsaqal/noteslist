import { TNote } from "./TNote";
export type TStack = {
  Home: undefined;
  NoteForm?: {
    Note: TNote;
  };
  NoteScreen: TNote;
};
//
