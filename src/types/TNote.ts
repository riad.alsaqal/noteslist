export type TNote = {
  id: string;
  title?: string;
  image: string;
  description: string;
  parent_id?: number;
};
