import Index from "./index";
import { Provider as PaperProvider } from "react-native-paper";
import { NavigationContainer as ReactNavigationProvider } from "@react-navigation/native";
import { DBProvider } from "./src/DBProvider";

export default function App() {
  return (
    <PaperProvider>
      <ReactNavigationProvider>
        <DBProvider>
          <Index />
        </DBProvider>
      </ReactNavigationProvider>
    </PaperProvider>
  );
}
